from flask import Flask, render_template
from flask_frozen import Freezer

# 1. Instantiate a class
app = Flask(__name__) #Create a Flask application instance with the name "app"
freezer = Freezer(app) # Create a Freezer Instance with the name "freezer"

# 2. Define Paths for Web pages using flask-frozen
#app.config['FREEZER_BASE_URL'] = 'https://joseramiezdevops.gitlab.io/' # Set up URL
app.config['FREEZER_DESTINATION'] = 'public' # Set folder to paste files

# 3. Define Custom functions (cli stands for Command Line Interface)
@app.cli.command("renderCustomCommand") # To define custom command "renderCustomCommand" . Command Line Interface in a composable way with as little code as necessary
def renderCustomCommand(): #custom command definiton to be able to upload files to public folder in the Website
    freezer.freeze()

# 4. Path Web requests/responses
@app.route('/index.html')
def displayHTML():
    return render_template('./index.html')

@app.route('/html2/Lab-1.html')
def displayHTML2():
    return render_template("./html2/lab-1.html")

@app.route('/html2/Lab-2.html')
def displayHTML3():
    return render_template("./html2/lab-2.html")

@app.route('/html2/Lab-3.html')
def displayHTML4():
    return render_template("./html2/lab-3.html")

@app.route('/html2/Lab-4.html')
def displayHTML5():
    return render_template("./html2/lab-4.html")